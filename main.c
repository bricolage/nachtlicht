 /*  Copyright(C) 2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define F_CPU 8000000L
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <string.h>

/* avr-libc 1.4.x !!!! */

#define BLINK_PORT PORTB
#define BLINK_DDR DDRB
#define BLINK_PIN _BV(PB0)
#include "blink.c"

#define PWM_PINS 5
#define PWM_OFFSET 1
#define PWM_PORT PORTB
#define PWM_DDR DDRB
#include "pwm.c"

volatile uint8_t kommando; //kommando das ueber i2c empfangen wurden
#define IRKOMMANDO kommando
#define IREEPSTART 0x0
#define IREEPEND 0xff
#include "ir.c"

#define DEINIT_INTERUPT deinit_pwm(); deinit_ir();
#define I2CKOMMANDO kommando
#define I2CBOOT512W
#define TWIADDR 0x06
#include "twi.c"

#define DIMSTEP 3


int 
main(void)
{
  BLINK_DDR |= BLINK_PIN; //LED pin auf ausgang
  init_twi();
  init_ir();
  uint8_t pwmled[]={255,255,255,255,255};
  sort(pwmled);
  init_pwm();
  
  sei();
  TWCR |= (1<<TWINT); //TWI-Modul aktiv (unbedingt nach sei() )
  
  kommando = 0x40;
  blink(5, 20, 20);
  if(kommando == 0x40){
    pwmled[0] = 0;
    pwmled[1] = 0;
    pwmled[2] = 0;
    pwmled[3] = 0;
    pwmled[4] = 0;
    sort(pwmled);
    //PORTB &= ~(LEDPINS);
    kommando = 0;
    keylen = eeprom_read_byte(keylen_addr);
    scantrigger = eeprom_read_byte(scantrigger_addr);
    keyanzahl = eeprom_read_byte(keyanzahl_addr);
  }
  
  uint8_t iraktion = 0;
  uint8_t i; //univeral zaehler var
  uint8_t countdown = 0;
  while(1) {
    switch(kommando){
    case 7:
      if (iraktion == 0)
        iraktion = 5;
      iraktion--;
      if (pwmled[iraktion] == 0)
        pwmled[iraktion] = DIMSTEP;
      i = pwmled[iraktion];
      pwmled[iraktion] = 0xFF;
      sort(pwmled);
      blink(3, 2, 10);
      pwmled[iraktion] = i;
      break;
    case 8:
      iraktion++;
      if (iraktion > 4)
        iraktion = 0;
      if (pwmled[iraktion] == 0)
        pwmled[iraktion] = DIMSTEP;
      i = pwmled[iraktion];
      pwmled[iraktion] = 0xFF;
      sort(pwmled);
      blink(3, 2, 10);
      pwmled[iraktion] = i;
      break;
    case 9:
      if(iraktion > PWM_PINS){
        for(i=0;i<PWM_PINS;i++){
          if(pwmled[i] > DIMSTEP)
            pwmled[i]-=DIMSTEP;
        }
      }
      else{
        pwmled[iraktion]-=DIMSTEP;
      }
      break;
    case 10:
      if(iraktion > PWM_PINS){
        for(i=0;i<PWM_PINS;i++){
          if(pwmled[i] < (0xff - DIMSTEP))
            pwmled[i]+=DIMSTEP;
        }
      }
      else{
        pwmled[iraktion]+=DIMSTEP;
      }
      break;
    case 5:
      pwmled[0] = 0xff;
      pwmled[1] = 0xff;
      pwmled[2] = 0xff;
      pwmled[3] = 0xff;
      pwmled[4] = 0xff;
      iraktion = PWM_PINS + 1;
      break;
    case 4:
      pwmled[0] = 255;
      pwmled[1] = 255;
      pwmled[2] = 1;
      pwmled[3] = 150;
      pwmled[4] = 120;
      break;
    case 6:
      pwmled[0] = 0;
      pwmled[1] = 0;
      pwmled[2] = 0;
      pwmled[3] = 0;
      pwmled[4] = 0;
      iraktion = PWM_PINS + 1;
      break;
    case 1:
      pwmled[0] = 255;
      pwmled[1] = 255;
      pwmled[2] = 5;
      pwmled[3] = 5;
      pwmled[4] = 5;
      break;
    case 2:
      pwmled[0] = 3;
      pwmled[1] = 3;
      pwmled[2] = 255;
      pwmled[3] = 1;
      pwmled[4] = 1;
      break;
    case 3:
      pwmled[0] = 5;
      pwmled[1] = 5;
      pwmled[2] = 1;
      pwmled[3] = 255;
      pwmled[4] = 255;
      break;
    case 0x30:
      deinit_pwm(); /* Int. fuer Timer ausschalten */
      break;
    case 0x31:
      init_pwm();
      break;
    case 0x3F:
      pwmled[0] = i2cbuf[1];
      pwmled[1] = i2cbuf[2];
      pwmled[2] = i2cbuf[3];
      pwmled[3] = i2cbuf[4];
      pwmled[4] = i2cbuf[5];
      break;
    }
    if (kommando != 0){
      sort(pwmled);
      kommando = 0;
      blink(1, 5, 1);
      countdown = 0;
    }
    else{
      if(countdown == 0xFF){
        uint8_t max = 0;
        for(i=0;i<PWM_PINS;i++){
          if(pwmled[i] > 0 )
            pwmled[i]--;
          if(pwmled[i] > max)
            max = pwmled[i];
        }
        sort(pwmled);
        if (max > 0){
          blink((0xff-max)/8, 25, 2);
        }
      }
      else{
        countdown++;
        blink(1,1,2);
      }
    }
    errortest_twi();
  }
  return 0;
}

