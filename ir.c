 /*  Copyright(C) 2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>

#include "blink.c"

#ifndef IREEPSTART
#define IREEPSTART 0x0
#endif
#ifndef IREEPEND
#define IREEPEND E2END
#endif


volatile uint8_t timerover;  //überlaufzaehler fuer den timer
volatile uint8_t timerstand; //zwischenspeicher für den timerstand
volatile uint8_t wertindex;  //zaehler fuer den ir-zeitabschnitt
volatile uint8_t scanindex;
volatile uint8_t scancount = 0;
volatile uint8_t scanmax;
volatile uint8_t scanmin_keysave;
volatile uint8_t scantrigger;
volatile uint8_t keyanzahl;
volatile uint8_t keylen;
uint8_t *keylen_addr = 0 + IREEPSTART;
uint8_t *scantrigger_addr = (uint8_t *) 1 + IREEPSTART;
uint8_t *keyanzahl_addr = (uint8_t *) 2 + IREEPSTART;
uint8_t *keystart_addr = (uint8_t *) 3 + IREEPSTART;
volatile uint8_t bitscan[8];

SIGNAL (SIG_INTERRUPT0) {
  /*  interrupt durch icl1 verarbeiten
  */
  timerstand = TCNT0; //zaehlerstand speichern
  TCNT0 = 0x00; // zaehler auf null init (ohne L/H da wir ja 16bit verarbeiten
  TCCR0 |= _BV(CS02); //prescaler einschalten auf 256
  uint8_t i = 0;
  
  if (wertindex == 0){
    while(i < sizeof(bitscan)){
      bitscan[i++] = 0x00;
    }
  }
  if (!(PIND & _BV(PD2))) {
      /* mode2 -> pulse */
#ifdef BLINK_PORT
    BLINK_PORT |= BLINK_PIN; //led anschalten wenn eine definiert ist
#endif
    if(IRKOMMANDO == 0x40){
      if(scancount > 4){
        if(timerstand > scanmax)
          scanmax=timerstand;
        if(timerstand < scanmin_keysave)
          scanmin_keysave=timerstand;
      }else{
        scanmin_keysave=0xFF;
        scanmax=0;
      }
      scancount++;
    }
    else if (scanindex < 64){
      if(timerstand > scantrigger){
        bitscan[scanindex/8] |= 1 << (scanindex % 8);
      }
      scanindex++;
    }
  }
  else {
#ifdef BLINK_PORT
    BLINK_PORT &= ~BLINK_PIN; // led ausschalten wenn das ir signal endet
#endif
  }
  wertindex ++;
  timerover = 0;
}

SIGNAL (SIG_OVERFLOW0) {
  /*  timer overflow verarbeiten
  */
  timerover ++;
  if (timerover > 1){
    TCCR0 &= ~_BV(CS02);
    TCNT0 = 0x00;
    uint8_t i = 0;
    uint8_t cmpbuf[8];
    while ( i < keyanzahl){
      eeprom_read_block(&cmpbuf, keystart_addr+((keylen/8+1)*i++),keylen/8+1);
      if(memcmp(&bitscan, &cmpbuf, (keylen/8+1)) == 0){
        IRKOMMANDO = i;
        break;
      }
    }
    if(IRKOMMANDO == 0x41){
      blink(1,20,10);
      uint8_t *eepkeyaddr = keystart_addr+((keylen/8+1)*(scanmin_keysave++));
      if ((((uint16_t) eepkeyaddr) + keylen/8+1) < IREEPEND){
        eeprom_write_block(&bitscan, (void *) eepkeyaddr, keylen/8+1);
        if(scanmin_keysave > keyanzahl){
          keyanzahl = scanmin_keysave;
          eeprom_write_byte(keyanzahl_addr, keyanzahl);
        }
        blink(scanmin_keysave, 3, 6);
      }
      else{
        IRKOMMANDO = 0x42;
        blink(20, 1, 2);
      }
    }
    else if(IRKOMMANDO == 0x40){
      if (scancount < 8){
        blink(15,3,2);
        IRKOMMANDO = 0;
      }
      else{
        keylen = scancount;
        scantrigger = ((scanmax - scanmin_keysave) / 2) + scanmin_keysave;
        eeprom_write_byte(keylen_addr, scancount);
        eeprom_write_byte(scantrigger_addr, scantrigger);
        eeprom_write_byte(keyanzahl_addr, 0);
        blink(3,5,5);
        IRKOMMANDO = 0x41;
        scanmin_keysave = 0;
      }
    }
    timerover = 0;
    wertindex = 0;
    scanindex = 0;
    scancount = 0;
  }
}

void
init_ir(void){
  /* Interrupt an INT0 aktivieren */
  TIMSK |= _BV(TOIE0); // capture interrupt einschalten und overflow int einschalten
  PORTD &= ~_BV(PD2); // pullup disabled (TSOP hat einen eingebaut)
  MCUCR &= ~_BV(ISC01); // ext. int. aktiv bei flanke an INT0
  MCUCR |= _BV(ISC00);
  GIFR |= _BV(INTF0); // clear ext. int. flag (clear heisst hier eine 1 schreiben !
  GICR |= _BV(INT0); // enable ext. int.
}

void
deinit_ir(void){
  TIMSK &= ~_BV(TOIE0); /* Int. fuer Timer ausschalten */
  GICR &= ~_BV(INT0); /* Externer Interrupt ausschalten */
}
