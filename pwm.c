 /* Copyright(C) 2005 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

/*
  Aufbau des Timetable's
{{Zeit, Bitmask},....}
*/

volatile uint8_t timetable[PWM_PINS][2];
volatile uint8_t i_timetable[PWM_PINS][2];
volatile uint8_t length;
volatile uint8_t i_length;
volatile uint8_t now = 0;
volatile uint8_t overflow_mask = 0;
volatile uint8_t i_overflow_mask = 0;
volatile uint8_t update_table = 0;
volatile uint8_t timer_overflow = 0;

void 
init_pwm(void) 
{
  PWM_DDR |= (0xff >> (8 - PWM_PINS)) << PWM_OFFSET;//PWM_LEDPINS;
  /* Normal PWM Mode */
  /* da gibts nichts zu setzen ;-) */
  /* 256 Prescaler */
  TCCR2 |=_BV(CS21) | _BV(CS22);
  /* Int. bei Overflow und CompareMatch einschalten */
  TIMSK |= _BV(TOIE2) | _BV(OCIE2); 
}

void
deinit_pwm(void)
{
  TIMSK &= ~(_BV(TOIE2)|_BV(OCIE2)); /* Int. fuer Timer ausschalten */
  PWM_DDR &= ~((0xff >> (8 - PWM_PINS)) << PWM_OFFSET);
}

SIGNAL(SIG_OUTPUT_COMPARE2) 
{
  if(i_length) {
    PWM_PORT &= ~i_timetable[now][1];
    if (++now < i_length)
      OCR2 = i_timetable[now][0];
  }  
}

SIGNAL(SIG_OVERFLOW2) 
{
  if(update_table == 1){
    uint8_t i;
    for (i=0; i < length; i++) {
      i_timetable[i][0] = timetable[i][0];
      i_timetable[i][1] = timetable[i][1];
    }
    i_length = length;
    i_overflow_mask = overflow_mask;
    update_table = 0;
  }
  OCR2 = i_timetable[0][0];
  if (!OCR2)
    i_timetable[1][0];
  now = 0;

  PWM_PORT |= i_overflow_mask;
  timer_overflow++;
}

void 
sort(uint8_t color[]) 
{
  uint8_t i,y,x = 0;
  uint8_t temp[PWM_PINS][2];
  /* Schauen ob schon vorhanden */
  for (i = 0; i < PWM_PINS; i++) {
    temp[i][0] = 0;
    temp[i][1] = 0;
    uint8_t vorhanden=0;
    for (y = 0; y < x; y++) {
      if (color[i] == temp[y][0])
        vorhanden = 1;
    }
    if (! vorhanden ) {
      temp[x][0] = color[i];
      x++;
    }
  }
  /* Sotieren */
  if (x != 1) {
    uint8_t t;
    for (y = 0; y < (x - 1); y++) {
      for (i = 0; i < (x-1) - y; i++) {
        if (temp[i][0] > temp[i+1][0]) {
          t = temp[i][0];
          //if(debug < 0x20)
          //  buf[debug++] = t;
          temp[i][0] = temp[i+1][0];
          temp[i+1][0] = t;
        }
      }
    }
  }
  /* Eintragen */
  for (i = 0; i < PWM_PINS; i++) {
    for(y = 0; y < x; y++) {
      if (color[i] == temp[y][0]) {
        temp[y][1] |= (1 << (i + PWM_OFFSET));
      }
    }
  }
  /* So etz noch die alten Werte ersetzen */
  while(update_table);
  for (i = 0; i < x; i++) {
    timetable[i][0] = temp[i][0];
    timetable[i][1] = temp[i][1];
  }
  //now = 0;
  //TCNT2 = 0;
  length=x;
  //OCR2=timetable[0][0];
  /* Overflow_mask neu bilden */
  overflow_mask = (0xff >> (8 - PWM_PINS)) << PWM_OFFSET;//0x07 << PWM_OFFSET;
  if ( timetable[0][0] == 0)
    overflow_mask &= ~timetable[0][1];
  if ( timetable[i_length - 1][0] == 0xff)
    i_length--;
  update_table = 1;
}
