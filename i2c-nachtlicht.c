/**********************************************************
 Copyright(C) 2006 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#define MAX_LINE_SIZE 1024
#define MEMORY_SIZE 512
#include <libsmbus.h>

typedef unsigned char byte;

void usage(void){
  fprintf(stderr,
          "Benutzung: i2cpwm [-d deviceid/pfad] [-r] [-led1 value] [-led2 value] [-led3 value] -kommando 0x3E,0x3F,0x3D,0x30,0x31\n");
  fprintf(stderr,"deviceid: -d 19640001\nusw\n");

}

int 
main(int argc, char **argv)
{
  if(argc < 2){
    usage();
    return 1;
  }

  unsigned int led[] = {0,0,0,0,0};
  unsigned int dimdelay = 100;
  unsigned int i2ckommando = 0;
  unsigned int readflash = 0;
  unsigned int i;
  //char device = "/dev/i2c-0";
  char *device = "19640001";
  //Filename = argv[1];
  //int delay = 0;
  byte recv[35];
  unsigned int recvlen;
  
  argv ++; /* skip first arg, which is our program name ... */
  while(*argv) {
    
    if(! strcmp(*argv, "-r")){
      readflash = 1;
      argv++;
      continue;
    }
    if(! strcmp(*argv, "-scan")){
      readflash = 2;
      argv++;
      continue;
    }

    if(! argv[1]) {
      usage();
      return 2;
    }
    if(! strcmp(*argv, "-kommando")){
      i2ckommando = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-led1")){
      led[0] = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-led2")){
      led[1] = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-led3")){
      led[2] = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-led4")){
      led[3] = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-led5")){
      led[4] = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    if(! strcmp(*argv, "-dimdelay")){
      dimdelay = (unsigned int) strtoul(argv[1], NULL, 0);
    }
    
    if(! strcmp(*argv, "-d"))
      device = argv[1];


    argv += 2;
  }

  /* open the i2c port */
  struct i2cdev *devhandle = smbus_open(device, 0x06);
  if(devhandle == NULL){ /* i2c_open already emitted an error message */
    fprintf(stderr,"Konnte Device nicht oeffnen\n");
    return 1;
  }
  if(i2ckommando > 0){
    byte i2cbuf[10];
    i2cbuf[0] = i2ckommando;
    i2cbuf[1] = led[0];
    i2cbuf[2] = led[1];
    i2cbuf[3] = led[2];
    i2cbuf[4] = led[3];
    i2cbuf[5] = led[4];
    i2cbuf[10] = dimdelay;
    if(smbus_send(devhandle, 0x40, (unsigned char *) &i2cbuf[0], sizeof(i2cbuf)) != sizeof(i2cbuf)) {
      fprintf(stderr, "couldn't successfully send data to avr.\n");
      return 1;
    }
    if(readflash == 1)
      usleep(50000);
  }
  /* flash auslesen und als intel hex format ausgeben */
  if(readflash > 0){
    if(smbus_recv(devhandle, 0x43+readflash, (unsigned char *) &recv[0], &recvlen) <= 0) {
      fprintf(stderr, "couldn't successfully recv data from avr.\n");
      return 1;
    }
    for(i=0;i<0x10;i++){
      printf("%02X ", recv[i]);
    }
    printf("\n");
    for(i=0x10;i<0x20;i++){
      printf("%02X ", recv[i]);
    }
    printf("\n");
  }
	return smbus_close(devhandle);
}

