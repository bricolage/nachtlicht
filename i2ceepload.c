/**********************************************************
 Copyright(C) 2006 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#define MAX_LINE_SIZE 1024
#define MEMORY_SIZE 512
#include <libsmbus.h>

typedef unsigned char byte;

void usage(void){
  fprintf(stderr,
    "Benutzung: i2ceppload [-d devnr] [-r Addresse] [-f file.hex] [-smbusdelay]\n");
  fprintf(stderr,"EEprom download: -r Startaddresse\nUploadhexfile: -f eeprom.hex\ni2c-Devicenummer: -d Nummer\nDelay (50ms) zwischen den Schreibvorgaengen erzwingen: -smbusdelay\n");

}

int 
main(int argc, char **argv)
{
  if(argc < 2){
    usage();
    return 1;
  }

  char Line[MAX_LINE_SIZE];
  char *Data;
  byte *Memory_Block;
  unsigned int Byte_Addr = 0;
  unsigned int Phys_AddrBegin = 0;
  unsigned int Phys_AddrEnde = 0;
  unsigned int readflash = 0;
  unsigned int i;
  unsigned int begin = 0;
  int PadByte = 0xFF;
  unsigned int  Anzahl_Bytes;
  unsigned int  Erstes_Wort;
  unsigned int  Type;
  byte  Checksum = 0;
  byte  Data_Str[MAX_LINE_SIZE];
  char *Filename = NULL;
  char *device = "19640001";
  //Filename = argv[1];
  int temp2;
  int delay = 0;
  byte recv[35];
  unsigned int recvlen;
  
  argv ++; /* skip first arg, which is our program name ... */
  while(*argv) {

    if(! strcmp(*argv, "-smbusdelay")){
      delay = 150;
      argv++;
      continue;
    }

    if(! argv[1]) {
      usage();
      return 2;
    }


    if(! strcmp(*argv, "-f"))
      Filename = argv[1];

    else if(! strcmp(*argv, "-r")){
      Phys_AddrBegin = (unsigned int) strtoul(argv[1], NULL, 0);
      readflash = 1;
    }

    if(! strcmp(*argv, "-d"))
      device = argv[1];


    argv += 2;
  }

  Memory_Block = (byte *) malloc(MEMORY_SIZE);
  if (Memory_Block == NULL){
    fprintf(stderr,"Malloc schlug fehl\n");
    return 1;
  }
  memset (Memory_Block,PadByte,MEMORY_SIZE);

  /* open the i2c port */
  struct i2cdev *devhandle = smbus_open(device, 0x06);
  if(devhandle == NULL){ /* i2c_open already emitted an error message */
    fprintf(stderr,"Konnte Device nicht oeffnen\n");
    return 1;
  }

  /* flash auslesen und als intel hex format ausgeben */
  if(readflash == 1){
    fprintf(stderr, "setze Zeiger auf Adresse %04x\n", Phys_AddrBegin);
    byte FlashAddr[2];
    while(Phys_AddrBegin < 0x1FF){
      //memcpy(&FlashAddr[4], Memory_Block, 64);
      //fwrite(FlashAddr, 2, 2, stdout);
      //fwrite(Memory_Block, 1, 64, stdout);
      FlashAddr[0]=Phys_AddrBegin & 0xFF;
      FlashAddr[1]=(Phys_AddrBegin>>8) & 0xFF;
      //fprintf(stderr, "setze zeiger auf Adresse %04x\n", Phys_AddrBegin);
      if(smbus_send(devhandle, 0x81, (unsigned char *) &FlashAddr[0], 2) != 2) {
        fprintf(stderr, "couldn't successfully send data to avr. sorry.\n");
        return 1;
      }
      if(delay > 0)
        usleep(delay*1000);
  
      if(smbus_recv(devhandle, 0x84, (unsigned char *) &recv[0], &recvlen) <= 0) {
        fprintf(stderr, "couldn't successfully recv data from avr. sorry.\n");
        return 1;
      }
      if(delay > 0)
        usleep(delay*1000);

      if(recvlen == 0x10){
        Checksum = recvlen + (Phys_AddrBegin >> 8) + (Phys_AddrBegin & 0xFF);
        printf(":10%04X00",Phys_AddrBegin);
        for(i=0;i<0x10;i++){
        printf("%02X", recv[i]);
        Checksum = (Checksum + recv[i]) & 0xFF;
        }
        printf("%02X\n", (0x100-Checksum) & 0xFF);
      }
      Phys_AddrBegin+=16;
    }

  }


if(Filename != NULL){
  FILE  *Hexfile;
  Hexfile = fopen(Filename,"r");
  if (Hexfile == NULL){
    fprintf(stderr,"Konnte Hexfile nicht oeffnen\n");
    return 1;
  }

  do /* repeat until EOF(Filin) */
      {
      /* Read a line from input file. */
      fgets(Line,MAX_LINE_SIZE,Hexfile);
  
      /* Remove carriage return/line feed at the end of line. */
      i = strlen(Line)-1;
  
      if (Line[i] == '\n') Line[i] = '\0';
  
      /* Scan the first two bytes and nb of bytes.
      The two bytes are read in First_Word since it's use depend on the
      record type: if it's an extended address record or a data record.
      */
      sscanf (Line, ":%2x%4x%2x%s",&Anzahl_Bytes,&Erstes_Wort,&Type,Data_Str);
      if(begin == 0){
        Phys_AddrBegin = Erstes_Wort;
        begin = 1;
      }
      if(Type == 0){
        Checksum = Anzahl_Bytes + (Erstes_Wort >> 8) + (Erstes_Wort & 0xFF) + Type;
    
        Data = (char *) Data_Str;
/*erst testen        if(Erstes_Wort != (Byte_Addr + Phys_AddrBegin){
          Byte_Addr = Erstes_Wort - Phys_AddrBegin;
      }*/
        for (i= Anzahl_Bytes; i > 0; i--){
          sscanf (Data, "%2x",&temp2);
          Data += 2;
          Memory_Block[Byte_Addr++] = temp2;
          Checksum = (Checksum + temp2) & 0xFF;
        };
        
        /* Read the Checksum value. */
        sscanf (Data, "%2x",&temp2);
        
        /* Verify Checksum value. */
        Checksum = (Checksum + temp2) & 0xFF;
        
        /*if ((Checksum != 0) && Enable_Checksum_Error){
          Status_Checksum_Error = TRUE;
        }*/
    
        //fprintf(stderr,"%i, %i, %i, %0x\n", Anzahl_Bytes, Erstes_Wort, Type, Checksum);
        //fprintf(stderr,"%s\n", Data_Str);
      }
  
    } while (!feof (Hexfile));
      Phys_AddrEnde = Byte_Addr + Phys_AddrBegin;
      if((Phys_AddrBegin % 16) == 0)

      fprintf(stderr,"Adressbegin: %i,Anzahl %i,Ende %i\n", Phys_AddrBegin,Byte_Addr,Phys_AddrEnde);
  
  
      //fwrite(Memory_Block, 1, Byte_Addr, stdout);
      byte FlashAddr[2];
      while(Phys_AddrBegin < Phys_AddrEnde){
        //memcpy(&FlashAddr[4], Memory_Block, 64);
        //fwrite(FlashAddr, 2, 2, stdout);
        //fwrite(Memory_Block, 1, 64, stdout);
        FlashAddr[0]=Phys_AddrBegin & 0xFF;
        FlashAddr[1]=(Phys_AddrBegin>>8) & 0xFF;
        fprintf(stderr, "schreibe an Adresse %04x\n", Phys_AddrBegin);
        if(smbus_send(devhandle, 0x81, (unsigned char *) &FlashAddr[0], 2) != 2) {
          fprintf(stderr, "couldn't successfully send data (Address) to avr. sorry.\n");
          return 1;
        }
        if(delay > 0)
          usleep(delay*1000);

        if(smbus_send(devhandle, 0x82, (unsigned char *) Memory_Block, 16) != 16) {
          fprintf(stderr, "couldn't successfully send data (Flashdata) to avr. sorry.\n");
          return 1;
        }
        
        /*for(i=0;i<16;i++)
        printf("%02X",Memory_Block[i]);
        printf("\n");*/
        if(delay > 0)
          usleep(delay*1000);
      
        //usleep(50000);
        //sleep(3);
        Checksum = 0;
        for(i=0;i<16;i++)
          Checksum = (Checksum + Memory_Block[i]) & 0xFF; 
  
        if(smbus_recv(devhandle, 0x83, (unsigned char *) &recv[0], &recvlen) <= 0) {
          fprintf(stderr, "couldn't successfully recv data from avr. sorry.\n");
          return 1;
        }
        if(delay > 0)
          usleep(delay*1000);

        if(Checksum != recv[0])
          fprintf(stderr,"Checksum Error bei %04x: %02x != %02x\n",Phys_AddrBegin, Checksum, recv[0]);

        Phys_AddrBegin+=16;
        Memory_Block+=16;
      }
      /*init der ir keys im atmega*/
      int i2ckommando = 0x4F;
      if(smbus_send(devhandle, 0x40, (unsigned char *) &i2ckommando, 1) != 1) {
        fprintf(stderr, "couldn't successfully send data to avr. sorry.\n");
        return 1;
      }
      if(delay > 0)
        usleep(delay*1000);

    }


  return 0;
}

