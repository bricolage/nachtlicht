PROJECT=main
MCU=atmega8
CC=avr-gcc
I2CDEV=19640001
OBJCOPY=avr-objcopy
CFLAGS=-g -mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues -W

$(PROJECT).hex: $(PROJECT) i2c-nachtlicht i2ceepload
	$(OBJCOPY) -R .eeprom -O ihex $< $@

$(PROJECT): $(PROJECT).o
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$(PROJECT).map $<

$(PROJECT).o: $(PROJECT).c pwm.c ir.c blink.c twi.c
	$(CC) $(CFLAGS) -c -o $@ $(PROJECT).c


$(PROJECT).s: $(PROJECT).c
	$(CC) $(CFLAGS) -S $<

i2c-nachtlicht: i2c-nachtlicht.c
	gcc -Wall -ggdb -o i2c-nachtlicht i2c-nachtlicht.c -lsmbus

i2ceepload: i2ceepload.c
	gcc -Wall -ggdb -o i2ceepload i2ceepload.c -lsmbus

clean:
	rm -f *.map *.hex $(PROJECT) *.o *.s *~ sort i2c-nachtlicht i2ceepload

load: $(PROJECT).hex
	i2cloader -d $(I2CDEV) -f $(PROJECT).hex -s F0F1 -e FF -ai2c 0x06 

loadfirst: $(PROJECT).hex
	i2cloader -d $(I2CDEV) -f $(PROJECT).hex -e FF -smbusdelay

loadsp12: $(PROJECT).hex
	sp12 -wpf $(PROJECT).hex -P255

loaddude: $(PROJECT).hex
	avrdude -p $(MCU) -U flash:w:$(PROJECT).hex -E vcc,noreset

fuse:
	avrdude -p m8 -U lfuse:w:0xE4:m -E vcc,noreset
