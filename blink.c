 /*  Copyright(C) 2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define F_CPU 8000000L
#include <util/delay.h>
#include <avr/io.h>
#include <inttypes.h>

#ifndef BLINK
#define BLINK

void
blink(uint8_t blinki, uint8_t puls, uint8_t pause){
  uint8_t pulstemp = puls;
  uint8_t pausetemp = pause;
  while(blinki > 0){
    pulstemp = puls;
    pausetemp = pause;
#ifdef BLINK_PORT
    BLINK_PORT |= BLINK_PIN;
#endif
    while(pulstemp > 0){
      _delay_ms(25);
      pulstemp--;
    }
#ifdef BLINK_PORT
    BLINK_PORT &= ~BLINK_PIN;
#endif
    while(pausetemp > 0){
      _delay_ms(25);
      pausetemp--;
    }
    blinki--;
  }
}

#endif
